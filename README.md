# Map - JavaScript (/ React)

The `Map`, which holds key - value pairs and remembers the original insertion order of the keys. Any value (both objects and primitive values) may be used as either a key or a value.

*Implementation was created for the purpose of its own potential expansion and customization (way to work with the map, access to items, its representation not only in text form, etc.). But in the case of JavaScript, it is possible to use an already integrated implementation, see: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map*

* The map is implemented as a one-dimensional (object) array. Contains methods for map work simulation. Two versions are available (`MapV1` and` MapV2`).

    * `MapV1`
        * Keys are used instead of item indexes.
        * `src/map/MapV1.js`
        * Sample in `src/example/AppV1.jsx`
    * `MapV2`
        * A 'classic' one-dimensional array into which objects that contain key and value attributes are inserted.
        * `src/map/MapV2.js`
        * Sample in `src/example/AppV2.jsx`

Basically, the only significant difference is in accessing items from a one-dimensional array perspective. In the case of the first version, the items are accessed only with keys, and iteration must be adapted accordingly. In the second version, you can access individual objects in the array using numeric indexes.

## Start

To start, just download, import and open this project. Then enter `npm install` and` npm start` in the terminal. Then type `http://localhost:3000/` in your web browser and open the console (F12), where you will see a listing of the above examples.