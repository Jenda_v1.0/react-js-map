import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppV1 from './example/AppV1';
import AppV2 from "./example/AppV2";

function App() {
    return (
        <div className="App">
            <h1>JS Map examples</h1>
            <h2>See the console (F12).</h2>

            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    Edit <code>src/App.js</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
            </header>

            <AppV1/>
            <AppV2/>
        </div>
    );
}

export default App;