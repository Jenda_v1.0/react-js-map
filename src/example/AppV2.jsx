import React, {Component} from "react";
import Map from "../map/MapV2";
import AppV1 from "./AppV1";

class AppV2 extends Component {

    componentDidMount() {
        AppV1.printEmptyLine();
        console.log("--------------------------------------------------------------");
        AppV1.printEmptyLine();
        console.log("Map example, version 2");


        const map = new Map();

        map.put("key 1", "value 1");
        map.put("key 2", "value 2");
        map.put("key 3", "value 3");


        AppV2.printSize(map);


        AppV2.printIsEmpty(map);


        AppV2.printKeys(map);


        AppV2.printValues(map);


        AppV2.printMapForEach(map);


        AppV2.printMapEntrySet(map);


        AppV1.printEmptyLine();
        console.log("Update 'key 1'");
        map.put("key 1", "edited value 1");
        AppV2.printMapForEach(map);


        AppV1.printEmptyLine();
        console.log("Clone");
        const clone = map.clone();
        AppV2.printMapForEach(clone);


        AppV1.printEmptyLine();
        console.log("Remove 'key 1'");
        map.remove("key 1");

        AppV2.printSize(map);

        AppV2.printMapForEach(map);


        AppV2.exampleContainsKeyAndValue(map, "key 1", "value 1");
        AppV2.exampleContainsKeyAndValue(map, "key 2", "value 2");


        AppV2.exampleGetOrDefault(map);


        AppV1.printEmptyLine();
        console.log("Create a new map to show 'putAll' method");
        const mapForPutAll = new Map();
        mapForPutAll.put("put 1", "put value 1");
        mapForPutAll.put("put 2", "put value 2");
        map.putAll(mapForPutAll);

        AppV2.printMapForEach(mapForPutAll);
        AppV2.printMapForEach(map);


        AppV1.printEmptyLine();
        console.log("toString");
        AppV1.printEmptyLine();
        console.log(map.toString());


        AppV1.printEmptyLine();
        console.log("toArray");
        AppV1.printEmptyLine();
        const array = map.toArray();
        console.log(array);


        AppV1.printEmptyLine();
        console.log("Clear");
        map.clear();
        AppV2.printSize(map);

        AppV2.printIsEmpty(map);
    };

    static printSize = map => {
        AppV1.printEmptyLine();
        console.log("Size: " + map.size());
    };

    static printIsEmpty = map => {
        AppV1.printEmptyLine();
        console.log("Is empty: " + map.isEmpty());
    };

    static printKeys = map => {
        AppV1.printEmptyLine();

        console.log("Keys");
        map.keys().forEach(key => {
            console.log(key);
        });
    };

    static printValues = map => {
        AppV1.printEmptyLine();

        console.log("Values");
        map.values().forEach(value => {
            console.log(value);
        });
    };

    static printMapForEach = map => {
        AppV1.printEmptyLine();

        console.log("Map (forEach)");

        for (let i = 0; i < map.size(); i++) {
            const item = map.getItem(i);
            console.log("Key: '" + item.key + "', Value: '" + item.value + "'");
        }
    };

    static printMapEntrySet = map => {
        AppV1.printEmptyLine();

        console.log("Map (entrySet)");

        function exampleFce(key, value) {
            const text = "Do something with key: '" + key + "' and value: '" + value + "'.";
            console.log(text);
        }

        map.entrySet(exampleFce);
    };

    static exampleContainsKeyAndValue = (map, key, value) => {
        AppV1.printEmptyLine();

        console.log("Contains key '" + key + "'");
        const resultKey = map.containsKey(key);
        console.log(resultKey);

        console.log("Contains value '" + value + "'");
        const resultValue = map.containsValue(value);
        console.log(resultValue);
    };

    static exampleGetOrDefault = map => {
        AppV1.printEmptyLine();

        const defaultValue = "Default value";

        console.log("Get or default, key 'key 1'");
        const result1 = map.getOrDefault("key 1", defaultValue);
        console.log(result1);

        AppV1.printEmptyLine();
        console.log("Get or default, key 'key 2'");
        const result2 = map.getOrDefault("key 2", defaultValue);
        console.log(result2);
    };

    render() {
        return (
            <>
            </>
        )
    }
}

export default AppV2;