import React, {Component} from "react";
import Map from "../map/MapV1";

class AppV1 extends Component {

    componentDidMount() {
        console.log("Map example, version 1");


        const map = new Map();

        map.put("key 1", "value 1");
        map.put("key 2", "value 2");
        map.put("key 3", "value 3");


        AppV1.printSize(map);


        AppV1.printIsEmpty(map);


        AppV1.printKeys(map);


        AppV1.printValues(map);


        AppV1.printMapForEach(map);


        AppV1.printMapEntrySet(map);


        AppV1.printEmptyLine();
        console.log("Update 'key 1'");
        map.put("key 1", "edited value 1");
        AppV1.printMapForEach(map);


        AppV1.printEmptyLine();
        console.log("Clone");
        const clone = map.clone();
        AppV1.printMapForEach(clone);


        AppV1.printEmptyLine();
        console.log("Remove 'key 1'");
        map.remove("key 1");

        AppV1.printSize(map);

        AppV1.printMapForEach(map);


        AppV1.exampleContainsKeyAndValue(map, "key 1", "value 1");
        AppV1.exampleContainsKeyAndValue(map, "key 2", "value 2");


        AppV1.exampleGetOrDefault(map);


        AppV1.printEmptyLine();
        console.log("Create a new map to show 'putAll' method");
        const mapForPutAll = new Map();
        mapForPutAll.put("put 1", "put value 1");
        mapForPutAll.put("put 2", "put value 2");
        map.putAll(mapForPutAll);

        AppV1.printMapForEach(mapForPutAll);
        AppV1.printMapForEach(map);


        AppV1.printEmptyLine();
        console.log("toString");
        AppV1.printEmptyLine();
        console.log(map.toString());


        AppV1.printEmptyLine();
        console.log("toArray");
        AppV1.printEmptyLine();
        const array = map.toArray();
        console.log(array);


        AppV1.printEmptyLine();
        console.log("Clear");
        map.clear();
        AppV1.printSize(map);

        AppV1.printIsEmpty(map);
    };

    static printSize = map => {
        AppV1.printEmptyLine();
        console.log("Size: " + map.size());
    };

    static printIsEmpty = map => {
        AppV1.printEmptyLine();
        console.log("Is empty: " + map.isEmpty());
    };

    static printKeys = map => {
        AppV1.printEmptyLine();

        console.log("Keys");
        map.keys().forEach(key => {
            console.log(key);
        });
    };

    static printValues = map => {
        AppV1.printEmptyLine();

        console.log("Values");
        map.values().forEach(value => {
            console.log(value);
        });
    };

    static printMapForEach = map => {
        AppV1.printEmptyLine();

        console.log("Map (forEach)");

        Object.keys(map.getMap()).forEach(key => {
            console.log("Key: '" + key + "', Value: '" + map.get(key) + "'");
        });
    };

    static printMapEntrySet = map => {
        AppV1.printEmptyLine();

        console.log("Map (entrySet)");

        function exampleFce(key, value) {
            const text = "Do something with key: '" + key + "' and value: '" + value + "'.";
            console.log(text);
        }

        map.entrySet(exampleFce);
    };

    static exampleContainsKeyAndValue = (map, key, value) => {
        AppV1.printEmptyLine();

        console.log("Contains key '" + key + "'");
        const resultKey = map.containsKey(key);
        console.log(resultKey);

        console.log("Contains value '" + value + "'");
        const resultValue = map.containsValue(value);
        console.log(resultValue);
    };

    static exampleGetOrDefault = map => {
        AppV1.printEmptyLine();

        const defaultValue = "Default value";

        console.log("Get or default, key 'key 1'");
        const result1 = map.getOrDefault("key 1", defaultValue);
        console.log(result1);

        AppV1.printEmptyLine();
        console.log("Get or default, key 'key 2'");
        const result2 = map.getOrDefault("key 2", defaultValue);
        console.log(result2);
    };

    static printEmptyLine = () => {
        console.log("\n");
    };

    render() {
        return (
            <>
            </>
        )
    }
}

export default AppV1;