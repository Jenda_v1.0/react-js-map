const Map = function () {
    this._map = [];
};


Map.prototype = {

    /**
     * Removes all of the mappings from this map. The map will be empty after this call returns.
     */
    clear: function () {
        this._map = {};
    },

    /**
     * Returns a shallow copy of this HashMap instance: the keys and values themselves are not cloned.
     *
     * <i>The method creates clone of the original array. Keep in mind that if objects exist in original array (this._map), the references are kept, i.e. the code does not do a "deep" clone of the array contents.</i>
     *
     * @returns {Map} copied instance.
     */
    clone: function () {
        const map = new Map();
        map.putAll(this);
        return map;
    },

    /**
     * Returns true if this map contains a mapping for the specified key.
     *
     * @param key The key whose presence in this map is to be tested
     * @returns {boolean} true if this map contains a mapping for the specified key.
     */
    containsKey: function (key) {
        return this._map.some(i => i.key === key);
    },

    /**
     * Returns true if this map maps one or more keys to the specified value.
     *
     * @param value value whose presence in this map is to be tested
     * @returns {boolean} true if this map maps one or more keys to the specified value
     */
    containsValue: function (value) {
        return this._map.some(i => i.value === value);
    },

    /**
     * 'Replacement' for 'entrySet'.
     *
     * <i>Iterates all items in the map and calls a function in the parameter ('callBack') for each iteration to pass the key and value parameters in the appropriate iteration.</i>
     *
     * @param callBack a function that is called in each iteration. The key and value ​​of the iterated item on the map are always passed to the callBack as it´s parameters.
     */
    entrySet: function (callBack) {
        this._map.forEach(item => callBack(item.key, item.value));
    },

    /**
     * Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
     *
     * More formally, if this map contains a mapping from a key 'key' to a value v such that (key==null ? k==null : key.equals(k)), then this method returns v; otherwise it returns null. (There can be at most one such mapping.)
     *
     * A return value of null does not necessarily indicate that the map contains no mapping for the key. It's also possible that the map explicitly maps the key to null. The containsKey operation may be used to distinguish these two cases.
     *
     * @param key the key whose associated value is to be returned
     * @returns the value to which the specified key is mapped, or null if this map contains no mapping for the key
     */
    get: function (key) {
        const index = this.getKeyIndex(key);
        return index === -1 ? null : this._map[index];
    },

    /**
     * Get an item in the map at the specified index.
     *
     * @param index index the item on the map to return
     * @returns item on the specified index on the map, otherwise undefined
     */
    getItem: function (index) {
        return this._map[index];
    },

    /**
     * Get an index of an item in a this._map which, as a key contains a value in the 'key' parameter.
     *
     * @param key The key whose presence in this map is to be tested
     * @returns {number} -1 if the key item in the 'key' parameter cannot be found, otherwise the index of the found item in the this._map array ('map').
     */
    getKeyIndex: function (key) {
        return this._map.findIndex(i => i.key === key);
    },

    /**
     * Returns the value to which the specified key is mapped, or defaultValue if this map contains no mapping for the key.
     *
     * <i>In the implementation, 'this.get (key)' could be used instead of 'this.getKeyIndex (key)', but it wouldn't know if the value 'null' is a return value from the map or returned from the method because the key was not found.</i>
     *
     * @param key the key whose associated value is to be returned
     * @param defaultValue the default mapping of the key
     * @returns the value to which the specified key is mapped, or defaultValue if this map contains no mapping for the key
     */
    getOrDefault: function (key, defaultValue) {
        const index = this.getKeyIndex(key);

        if (index === -1) {
            return defaultValue;
        }

        return this._map[index].value;
    },

    /**
     * Returns true if this map contains no key-value mappings.
     *
     * @returns true if this map contains no key-value mappings
     */
    isEmpty: function () {
        return this.size() === 0;
    },

    /**
     * Returns a array of the keys contained in this map. The array is not related to the map, so no changes to the map are reflected in the array, and vice-versa.
     *
     * @returns {Array} a array of the keys contained in this map
     */
    keys: function () {
        const keys = [];
        this._map.forEach(item => keys.push(item.key));
        return keys;
    },

    /**
     * Associates the specified value with the specified key in this map. If the map previously contained a mapping for the key, the old value is replaced.
     *
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     */
    put: function (key, value) {
        const index = this.getKeyIndex(key);

        if (index === -1) {
            const item = {
                key: key,
                value: value
            };
            this._map.push(item);
        } else {
            this._map[index].value = value;
        }
    },

    /**
     * Copies all of the mappings from the specified map (in parameter) to this map (this._map). These mappings will replace any mappings that this map had for any of the keys currently in the specified map.
     *
     * <i>The 'map' parameter must be an array of objects with 'key' and 'value' attributes.</i>
     *
     * @param map mappings to be stored in this map
     */
    putAll: function (map) {
        map._map.forEach(item => {
            this.put(item.key, item.value);
        });
    },

    /**
     * Removes the mapping for the specified key from this map if present.
     *
     * @param key key whose mapping is to be removed from the map
     * @returns the previous value associated with key, or null if there was no mapping for key. (A null return can also indicate that the map previously associated null with key.)
     */
    remove: function (key) {
        const index = this.getKeyIndex(key);

        if (index === -1) {
            return null;
        }

        const value = this.get(key);
        this._map.splice(index, 1);
        return value;
    },

    /**
     * Returns the number of key-value mappings in this map.
     *
     * @returns {number} The number of key-value mappings in this map
     */
    size: function () {
        return Object.keys(this._map).length;
    },

    /**
     * Returns an array containing all of the elements in this map in proper sequence (from first to last element).
     *
     * @returns {Array} an array containing all of the elements in this map in proper sequence
     */
    toArray: function () {
        const array = [];

        this._map.forEach(item => {
            const key = item.key;
            const value = item.value;
            const mapItem = {key, value};
            array.push(mapItem);
        });

        return array;
    },

    /**
     * Returns a string representation of this map.
     *
     * @returns a string representation of this map.
     */
    toString: function () {
        let mapInString = "{";

        this._map.forEach(item => mapInString = mapInString.concat(item.key + "=" + item.value + ", "));

        mapInString = mapInString.substring(0, mapInString.length - 2);
        mapInString = mapInString.concat("}");

        return mapInString;
    },

    /**
     * Returns a array of the values contained in this map. The array is not related to the map, so no changes to the map are reflected in the array, and vice-versa.
     *
     * @returns {Array} a array of the values contained in this map
     */
    values: function () {
        const values = [];
        this._map.forEach(item => values.push(item.value));
        return values;
    }
};

export default Map;