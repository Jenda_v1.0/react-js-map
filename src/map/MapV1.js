const Map = function () {
    this._map = {};
};


Map.prototype = {

    /**
     * Removes all of the mappings from this map. The map will be empty after this call returns.
     */
    clear: function () {
        this._map = {};
    },

    /**
     * Returns a shallow copy of this HashMap instance: the keys and values themselves are not cloned.
     *
     * <i>The method creates clone of the original array. Keep in mind that if objects exist in original array (this._map), the references are kept, i.e. the code does not do a "deep" clone of the array contents.</i>
     *
     * @returns {Map} copied instance.
     */
    clone: function () {
        const map = new Map();
        map.putAll(this);
        return map;
    },

    /**
     * Returns true if this map contains a mapping for the specified key.
     *
     * @param key The key whose presence in this map is to be tested
     * @returns {boolean} true if this map contains a mapping for the specified key.
     */
    containsKey: function (key) {
        return this._map.hasOwnProperty(key);
    },

    /**
     * Returns true if this map maps one or more keys to the specified value.
     *
     * @param value value whose presence in this map is to be tested
     * @returns {boolean} true if this map maps one or more keys to the specified value
     */
    containsValue: function (value) {
        for (let key in this._map) {
            if (this._map.hasOwnProperty(key)) {
                if (this._map[key] === value) {
                    return true;
                }
            }
        }
        return false;
    },

    /**
     * 'Replacement' for 'entrySet'.
     *
     * <i>Iterates all items in the map and calls a function in the parameter ('callBack') for each iteration to pass the key and value parameters in the appropriate iteration.</i>
     *
     * @param callBack a function that is called in each iteration. The key and value ​​of the iterated item on the map are always passed to the callBack as it´s parameters.
     */
    entrySet: function (callBack) {
        Object.keys(this._map).forEach(key => {
            const value = this.get(key);
            callBack(key, value);
        });
    },

    /**
     * Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
     *
     * More formally, if this map contains a mapping from a key 'key' to a value v such that (key==null ? k==null : key.equals(k)), then this method returns v; otherwise it returns null. (There can be at most one such mapping.)
     *
     * A return value of null does not necessarily indicate that the map contains no mapping for the key. It's also possible that the map explicitly maps the key to null. The containsKey operation may be used to distinguish these two cases.
     *
     * @param key the key whose associated value is to be returned
     * @returns the value to which the specified key is mapped, or null if this map contains no mapping for the key
     */
    get: function (key) {
        return this.containsKey(key) ? this._map[key] : null;
    },

    /**
     * 'Replacement' for the ability to cycle through the map.
     *
     * <i>The method is not necessarily needed, the variable '_map' can be accessed as well as a public attribute.</i>
     *
     * @returns fields that contain key-indexes and key-mapped values
     */
    getMap: function () {
        return this._map;
    },

    /**
     * Returns the value to which the specified key is mapped, or defaultValue if this map contains no mapping for the key.
     *
     * @param key the key whose associated value is to be returned
     * @param defaultValue the default mapping of the key
     * @returns the value to which the specified key is mapped, or defaultValue if this map contains no mapping for the key
     */
    getOrDefault: function (key, defaultValue) {
        return this.containsKey(key) ? this._map[key] : defaultValue;
    },

    /**
     * Returns true if this map contains no key-value mappings.
     *
     * @returns true if this map contains no key-value mappings
     */
    isEmpty: function () {
        return this.size() === 0;
    },

    /**
     * Returns a array of the keys contained in this map. The array is not related to the map, so no changes to the map are reflected in the array, and vice-versa.
     *
     * @returns {Array} a array of the keys contained in this map
     */
    keys: function () {
        const keys = [];
        for (let key in this._map) {
            if (this._map.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
        return keys;
    },

    /**
     * Associates the specified value with the specified key in this map. If the map previously contained a mapping for the key, the old value is replaced.
     *
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     */
    put: function (key, value) {
        this._map[key] = value;
    },

    /**
     * Copies all of the mappings from the specified map (in parameter) to this map (this._map). These mappings will replace any mappings that this map had for any of the keys currently in the specified map.
     *
     * @param map mappings to be stored in this map
     */
    putAll: function (map) {
        Object.keys(map.getMap()).forEach(key => {
            const value = map.get(key);
            this.put(key, value);
        });
    },

    /**
     * Removes the mapping for the specified key from this map if present.
     *
     * @param key key whose mapping is to be removed from the map
     * @returns the previous value associated with key, or null if there was no mapping for key. (A null return can also indicate that the map previously associated null with key.)
     */
    remove: function (key) {
        if (this.containsKey(key)) {
            const value = this._map[key];
            delete this._map[key];
            return value;
        }
        return null;
    },

    /**
     * Returns the number of key-value mappings in this map.
     *
     * @returns {number} The number of key-value mappings in this map
     */
    size: function () {
        return Object.keys(this._map).length;
    },

    /**
     * Returns an array containing all of the elements in this map in proper sequence (from first to last element).
     *
     * @returns {Array} an array containing all of the elements in this map in proper sequence
     */
    toArray: function () {
        const array = [];

        Object.keys(this._map).forEach(key => {
            const value = this.get(key);
            const mapItem = {key, value};
            array.push(mapItem);
        });

        return array;
    },

    /**
     * Returns a string representation of this map.
     *
     * @returns a string representation of this map.
     */
    toString: function () {
        let mapInString = "{";

        Object.keys(this._map).forEach(key => {
            const value = this.get(key);
            mapInString = mapInString.concat(key + "=" + value + ", ")
        });

        mapInString = mapInString.substring(0, mapInString.length - 2);
        mapInString = mapInString.concat("}");

        return mapInString;
    },

    /**
     * Returns a array of the values contained in this map. The array is not related to the map, so no changes to the map are reflected in the array, and vice-versa.
     *
     * @returns {Array} a array of the values contained in this map
     */
    values: function () {
        const values = [];
        for (let key in this._map) {
            if (this._map.hasOwnProperty(key)) {
                values.push(this._map[key]);
            }
        }
        return values;
    }
};

export default Map;